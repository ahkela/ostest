let url = "https://5dd272c56625890014a6da46.mockapi.io/dogs"

exports.getDogs = () => {
    return fetch(url)
    .then(response => response.json())
    .then(responseJson => {
        return responseJson.data
    })
}