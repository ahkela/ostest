import React, {PureComponent} from 'react';
import {View, StyleSheet, Image, FlatList, TouchableOpacity} from 'react-native'
import { Text, Card, CardItem, Button, Icon, Left, Body, Right } from 'native-base';
import api from '../../assets/api'

export default class HomeScreen extends PureComponent {
    constructor(props){
        super(props)
        this.state = {
            data: []
        }
    }

    componentDidMount(){
        api.getDogs()
        .then(response => {
            if(response.length > 0){
                this.setState({
                    data: response
                })
            }
        })
        .catch(err => console.log(err))
    }

    renderCard = ({item}) => {
        return(
            <TouchableOpacity style={{flex: 1/2}}
                onPress={() => this.props.navigation.navigate("Details", {
                    details: item
                })}
            >
                <Card>
                    <CardItem cardBody>
                        <Image source={{uri: item.image}} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                    <CardItem>
                        <Left>
                            <Body>
                                <Text>{item.name != null ? item.name : item.description}</Text>
                                <Text note>{item.name != null ? item.description.length > 15 ? String(item.description.substring(0,13).concat("...")) : item.description : null}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        )
    }

    render(){
        return(
            <View>
                <FlatList
                    numColumns={2}
                    data={this.state.data}
                    renderItem={this.renderCard}
                    keyExtractor={(item, index) => String(index)}
                    contentContainerStyle={styles.list}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    list:{
        paddingVertical: 20,
        paddingHorizontal: 5
    },
})