import React, {PureComponent} from 'react';
import {Image, StyleSheet} from 'react-native'
import { Container, Content, Text } from 'native-base';

export default class DetailsScreen extends PureComponent {
    constructor(props){
        super(props)
        let temp = this.props.route.params.details
        this.state = {
            name: temp.name == null ? temp.description : temp.name,
            description: temp.name != null ? temp.description : "No description for this dog",
            image: temp.image
        }
        this.props.navigation.setOptions({
            headerTitle: "Details"
        })
    }

    render(){
        return(
            <Container>
                <Image source={{uri: this.state.image}} 
                    style={{height:400, width: null}}
                    resizeMode={"stretch"}
                />
                <Content style={styles.descriptionBox}>
                    <Text style={styles.title}>
                        {this.state.name}
                    </Text>
                    <Text note style={styles.description}>{this.state.description}</Text>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    descriptionBox:{
        paddingVertical: 10,
        paddingHorizontal: 15,

    },
    title: {
        fontWeight: '700'
    },
    description: {
        textAlign: 'justify',
        fontWeight: '500',
        paddingVertical: 10
    }
})